# beep

PC-speaker beeper. https://github.com/spkr-beep/beep

# Unofficial documentation
* [linux beep system speaker](https://google.com/search?q=linux+beep+system+speaker)
* [*How do I make my pc speaker beep*
  ](https://unix.stackexchange.com/questions/1974/how-do-i-make-my-pc-speaker-beep)

# Using kernel modules
    modprobe -r pcspkr ; modprobe snd-pcsp ; beep

# See also [SoX](https://gitlab.com/software-packages-demo/sox).